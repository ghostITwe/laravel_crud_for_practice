<?php

namespace Database\Seeders;

use App\Models\DeliveryType;
use Illuminate\Database\Seeder;

class DeliveryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deliveryType[0] = new DeliveryType();
        $deliveryType[0]->name = 'Самовывоз';
        $deliveryType[0]->save();

        $deliveryType[1] = new DeliveryType();
        $deliveryType[1]->name = 'Курьерская доставка';
        $deliveryType[1]->save();
    }
}
