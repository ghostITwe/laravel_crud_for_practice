<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles[0] = new Role();
        $roles[0]->name = 'admin';
        $roles[0]->description = 'Администратор';
        $roles[0]->save();

        $roles[1] = new Role();
        $roles[1]->name = 'manager';
        $roles[1]->description = 'Менеджер';
        $roles[1]->save();

        $roles[2] = new Role();
        $roles[2]->name = 'client';
        $roles[2]->description = 'Клиент';
        $roles[2]->save();
    }
}
