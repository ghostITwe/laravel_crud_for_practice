<?php

namespace Database\Seeders;

use App\Models\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentType[0] = new PaymentType();
        $paymentType[0]->name = 'Наличными';
        $paymentType[0]->save();

        $paymentType[1] = new PaymentType();
        $paymentType[1]->name = 'Картой при получении';
        $paymentType[1]->save();

        $paymentType[2] = new PaymentType();
        $paymentType[2]->name = 'Электронные деньги';
        $paymentType[2]->save();
    }
}
