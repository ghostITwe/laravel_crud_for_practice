<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category[0] = new Category();
        $category[0]->name = 'Телефоны';
        $category[0]->save();

        $category[1] = new Category();
        $category[1]->name = 'Видеокарты';
        $category[1]->save();

        $category[2] = new Category();
        $category[2]->name = 'Процессоры';
        $category[2]->save();

        $category[3] = new Category();
        $category[3]->name = 'Система охлаждения';
        $category[3]->save();

        $category[4] = new Category();
        $category[4]->name = 'Материнские платы';
        $category[4]->save();

        $category[5] = new Category();
        $category[5]->name = 'Блоки питания';
        $category[5]->save();
    }
}
