<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin[0] = new User();
        $admin[0]->surname = 'Ильин';
        $admin[0]->first_name = 'Владислав';
        $admin[0]->email = 'vladislav.ilyinr@gmail.com';
        $admin[0]->login = 'admin';
        $admin[0]->password = bcrypt('password');
        $admin[0]->save();

        $admin[1] = new User();
        $admin[1]->surname = 'Бурбах';
        $admin[1]->first_name = 'Владимир';
        $admin[1]->email = 'volodya@example.com';
        $admin[1]->login = 'manager';
        $admin[1]->password = bcrypt('password');
        $admin[1]->save();
    }
}
