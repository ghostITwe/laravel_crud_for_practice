<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product', function (Blueprint $table) {
            $table->bigInteger('order_id')->nullable()->unsigned()->comment('Заказ');
            $table->bigInteger('product_id')->nullable()->unsigned()->comment('Товар');
            $table->foreign('order_id')->references('id')->on('orders')
            ->onDelete('set null')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropDatabaseIfExists('order_product');
    }
}
