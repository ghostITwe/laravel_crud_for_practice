<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable()->unsigned()->comment('id Клиента');
            $table->bigInteger('delivery_type_id')->nullable()->unsigned()->comment('Тип доставки');
            $table->bigInteger('payment_type_id')->nullable()->unsigned()->comment('Тип оплаты');
            $table->date('date_order')->comment('Дата заказа');
            $table->string('delivery_address')->comment('Адрес доставки');
            $table->double('amount')->comment('Сумма заказа');
            $table->boolean('status')->nullable()->default(0)->comment('Статус заказа');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
