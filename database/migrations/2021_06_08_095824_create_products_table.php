<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Название товара');
            $table->double('price')->comment('Цена');
            $table->integer('count')->comment('Количество товара');
            $table->string('description')->nullable()->comment('Описание товара');
            $table->bigInteger('manufacturer_id')->nullable()->unsigned()->comment('Производитель');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
